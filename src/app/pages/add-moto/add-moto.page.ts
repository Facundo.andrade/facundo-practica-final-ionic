import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MotoService } from 'src/app/services/data.service';

@Component({
  selector: 'app-add-moto',
  templateUrl: './add-moto.page.html',
  styleUrls: ['./add-moto.page.scss'],
})
export class AddMotoPage implements OnInit {
  constructor(private data: MotoService, private router: Router) { }

  pngmoto: any = "../../../assets/shapes.svg"
  marca: ""
  modelo: ""
  year: ""
  precio: ""
  foto:''

  ngOnInit() {
  }

  subirFoto(fotopng) {

    this.foto=fotopng[0]
    console.log(fotopng);
    const lector = new FileReader();
    lector.readAsDataURL(fotopng[0]);
    lector.onload = (_event) => {
      this.pngmoto = lector.result;
    }

  }

  addM() {

    console.log("entro")

    const fData = new FormData()

    fData.append('marca',this.marca)
    fData.append('modelo',this.modelo)
    fData.append('year',this.year)
    fData.append('precio',this.precio)
    fData.append('foto',this.foto)
 
    this.data.subirMoto(fData)
    
    this.router.navigateByUrl('/home');

  }

  

}
