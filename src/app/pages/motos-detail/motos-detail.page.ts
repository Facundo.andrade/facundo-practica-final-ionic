import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-motos-detail',
  templateUrl: './motos-detail.page.html',
  styleUrls: ['./motos-detail.page.scss'],
})
export class MotosDetailPage implements OnInit {

  data:any;
  data2:any;
  data3:any;
  data4:any;
  data5:any;

  constructor(private route: ActivatedRoute, private router: Router, public alertController: AlertController) {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.data = this.router.getCurrentNavigation().extras.state.parametros;
        this.data2 = this.data.foto;
        this.data3 = this.data.modelo;
        this.data4 = this.data.marca;
        this.data5 = this.data.id;
      }
    });
  }
  ngOnInit(): void {
    
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Confirm!',
      message: 'EStas seguro que quieres eliminar la moto?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Si',
          handler: () => {
            console.log('Confirm Okay');
            this.deleteMoto(this.data5);
            
          }
        }
      ]
    });

    await alert.present();
  }

  hola(){

    console.log("asdasdasdas");

  }

  deleteMoto(idMoto) {
    const url = "http://facundo-andrade-7e3.alwaysdata.net/miapi/getmotos/" + idMoto;
    fetch(url, {
      "method": "DELETE"
    })
    .then(response => {
        this.router.navigateByUrl('/home');
      });
  }


}



