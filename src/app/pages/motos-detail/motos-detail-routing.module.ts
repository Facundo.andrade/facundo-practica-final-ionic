import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MotosDetailPage } from './motos-detail.page';

const routes: Routes = [
  {
    path: '',
    component: MotosDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MotosDetailPageRoutingModule {}
