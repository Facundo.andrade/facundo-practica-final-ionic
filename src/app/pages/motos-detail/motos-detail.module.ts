import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MotosDetailPageRoutingModule } from './motos-detail-routing.module';

import { MotosDetailPage } from './motos-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MotosDetailPageRoutingModule
  ],
  declarations: [MotosDetailPage]
})
export class MotosDetailPageModule {}
