import { MotoService, SearchType } from '../../services/data.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
 
@Component({
  selector: 'app-motos',
  templateUrl: './motos.page.html',
  styleUrls: ['./motos.page.scss'],
})
export class MotosPage implements OnInit {
 
  results: Observable<any>;
  searchTerm: string = '';
  type: SearchType = SearchType.all;
 
  /**
   * Constructor of our first page
   * @param movieService The movie Service to get data
   */
  constructor(private motoService: MotoService) { }
 
  ngOnInit() {
    
   }
 
  searchChanged() {
    // Call our service function which returns an Observable
    this.results = this.motoService.searchData(this.searchTerm, this.type);
  }
}
