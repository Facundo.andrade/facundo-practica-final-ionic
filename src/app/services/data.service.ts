import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
 
// Typescript custom enum for search types (optional)
export enum SearchType {
  all = '',
  ducati = 'Ducati',
  yamaha = 'Yamaha'
}
 
@Injectable({
  providedIn: 'root'
})
export class MotoService {
  [x: string]: any;
  url = 'http://facundo-andrade-7e3.alwaysdata.net/miapi';
  //url = 'http://localhost:3000/miapi'
  
  apiKey = ''; // <-- Enter your own key here!
 
  /**
   * Constructor of the Service with Dependency Injection
   * @param http The standard Angular HttpClient to make requests
   */
  constructor(private http: HttpClient) { }
 
  /**
  * Get data from the OmdbApi 
  * map the result to return only the results that we need
  * 
  * @param {string} title Search Term
  * @param {SearchType} type movie, series, episode or empty
  * @returns Observable with the search results
  */
  searchData(title: string, type: SearchType): Observable<any> {
    return this.http.get(`${this.url}?s=${encodeURI(title)}&type=${type}&apikey=${this.apiKey}`).pipe(
      map(results => results['Search'])
    );
  }
 
  /**
  * Get the detailed information for an ID using the "i" parameter
  * 
  * @param {string} id imdbID to retrieve information
  * @returns Observable with detailed information
  */
  getDetails(id) {
    return this.http.get(`${this.url}?i=${id}&plot=full&apikey=${this.apiKey}`);
  }

  async deleteMoto(idMoto) {
    
    const info=await fetch(`${this.url}/getmotos/${idMoto}`,{
      method:"DELETE",
    })
    return info
  }

  async getMotos(){
    const info=await (await fetch(`${this.url}/getmotos`)).json()
    return info
  }

  async subirMoto(formData){
    console.log(formData)
    const info=await fetch(`${this.url}/postmotos`,{
      method: "POST",
      body:formData
    })

    console.log(await info.json())
  }

  async getMotosMarca(marca:string){
    const info=await (await fetch(`${this.url}/getmotos?marca=${marca}`)).json()
    return info
  }
}