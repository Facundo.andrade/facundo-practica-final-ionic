import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'motos',
    loadChildren: () => import('./pages/motos/motos.module').then( m => m.MotosPageModule)
  },
  {
    path: 'motos-detail',
    loadChildren: () => import('./pages/motos-detail/motos-detail.module').then( m => m.MotosDetailPageModule)
  }, 
  {
    path: 'add-moto',
    loadChildren: () => import('./pages/add-moto/add-moto.module').then( m => m.AddMotoPageModule)
  },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
