import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { MotoService } from 'src/app/services/data.service';
import { MenuComponent } from 'src/app/components/menu/menu.component'


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  constructor(private router: Router, private data:MotoService) {}
 
  motos = [];
  marcas = [
    {
      title:"Ducati",
      foto:"https://upload.wikimedia.org/wikipedia/commons/6/66/Ducati_red_logo.PNG"
    },
    {
      title:"Honda",
      foto:"https://e00-marca.uecdn.es/assets/multimedia/imagenes/2016/12/30/14830895054212.jpg"
    },
    {
      title:"Yamaha",
      foto:"https://www.electromotos.net/wp-content/uploads/2018/01/yamaha-600x400.png"
    },
    {
      title:"Todas",
      foto: "https://www.pngfind.com/pngs/m/365-3650578_motorcycle-motorbike-bumper-dibujos-de-motos-ninjas-hd.png"
      
    }
  ]
  

  ionViewDidEnter(){
    this.ngOnInit()
  }

  ngOnInit() {
    (async ()=>{
      this.motos= await this.data.getMotos()
    })()
  }

  filtroMotos(e){

    (async () => {
      if(e!="Todas") this.motos= await this.data.getMotosMarca(e)
      else this.motos= await this.data.getMotos()
    })()
  }

  addMoto(){

    this.router.navigate(['add-moto'])

  }

  pasoParametro(moto){

    let navigationExtras: NavigationExtras = {
      state: {
        parametros: moto
      }
    };
    this.router.navigate(['motos-detail'], navigationExtras);
  }

}
