import { Component, Input, OnInit } from '@angular/core';
import { HomePage } from 'src/app/home/home.page';
import { MotoService } from 'src/app/services/data.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})

export class MenuComponent implements OnInit {

  moto:string;

  @Input() home:HomePage

  constructor(private data:MotoService) { }

  ngOnInit() {}

  onclick(e){
    this.moto = e;

  }

}
